#imports all the required modules needed for this script to run
import sqlite3 # used for creating and managing the database
from contextlib import closing # used for closing files
import os # imports system keywords
from os.path import exists # used for checking if the a table as already been created
import array # honestly no sure but im too scared to remove it incase it breaks anything I haven't got anytime left to fix
import random # allows for a randomise number to be picked
import codecs # replaces any unknown encoding into utf-8 to avoid any errors
import sys # allows for a clean exit of the program

# defines all the global varibles used through this program
global name # holds the name of a task
global due  # holds the due date of a task
global urgent # holds the urganance of a task
global exit1

connection = sqlite3.connect("tasks.db") # connects to the database
cursor = connection.cursor() # creates a cursor connection


#createtable
#if name == main:
def createtable():
        cursor.execute("CREATE TABLE tasks (taskID INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, due INTEGER, urgent TEXT)") #creates a table called tasks with a primary key called taskID that autoincrements with every
     # row created and the colloms name,due,urgant
        connection.commit()

def checktable(): # checks if the table exists if not calls the create table function
    print("setting up       ‎[          ]0%")
    try: createtable()  # calls the function to create the table called tasks
    except: sqlite3.OperationalError; # if this error occours it means the table already exists so we then know we don't have to make one
    print("set up succesful ‎[**********]100%")

checktable()
#create
def create(): # creates a collum within the table then prompts the user to input the data
    name = (input("Enter a name for your task: "))[:29] # asks user for the name of their task and saves it into name and limits the saved string length to 29 charters

    due = (input("Enter a due date: "))[:20] #does the same but for the due date can be either text or numbers to support maxium user customaility but limited to 20 chacters


    urgent = (input("Enter an urgency: "))[:15] #does the same but for the due date can be either text or numbers to support maxium user customaility
    cursor.execute("insert into tasks (name, due, urgent) values (?, ?, ?)", (name, due, urgent)) # inserts into the values of the varibles above then writes to the table
    connection.commit() # commits the data to the database
    print("data created") # informs the user that the data created

#delete deletes the specified colloum from the table
# need to add error handling when you input a task id that doesn't exist it crashes also crashes if you put a task id in of more than two digits simple if statement will properly do the trick
def delete(): # deletes the task the user as entered
    deleted = input("please enter taskid of task you like to delete:")[:3] # asks the user what the taskid is of the task they want deleted and limits the saved string length to 3 charters since its unlikely someone will have 1000+ tasks
    cursor.execute("DELETE FROM tasks WHERE taskID = ?", #deletes user inputted task according to which task id is stored within the variable deleted which is above
        (deleted),)

    connection.commit() # commits the charges to the database

    print("data deleted") # informs the user the data has been deleted

#view

def view(): # allows the user to view all existing colloums within the table
    rows = cursor.execute("SELECT * FROM tasks").fetchall() # selects everything from tasks and fetches it all saves it all to rows
    print("-----------------------------")

    print("task_id | name   | due | urgent") # all set to lowercase for user convenant
    for row in rows :

     print("   "+ str(row[0])+"      " +row[1]+"  " ,str(row[2])+" ",row[3]) # prints the values of rows NEEDS TIDYING UP so information matches names

     print("-----------------------------")


#edit
def edit(): # allows the user to edit a prexisting colloum within the table
    view() # calls the view function to handle the searching of tasks
    search = input("please enter the taskid of the task you want to edit? ")[:7] # asks the user what task they want to edit need and limits the string length to 3 to avoid possible sql injection attacks along side keyword migagation below
    value = 1 # avoids a assignment error by defining the varible before it referenced
    searchSQL = cursor.execute("SELECT taskID from tasks WHERE taskID = ?", (search,)).fetchone() # selects the tasksID from the table tasks if it

    if searchSQL != None: # asks the user what task they would like to like to edit and which part of the task they would like to edit once one is selected selects
        cri = str(input("what part would you like to edit?"))[:7] # that task from the database needs a if statement for error handling and limits the saved string length to 32 charaters


        # if cri is equal to any of the keywords then it prints incorrect and called the edit funcation again if they aren't equal doesn't continutes to the next stage
        keywords = ["name", "due", "urgent"]
        if cri not in keywords: # if the user enter string does not match any string listed in the keywords array then prints the string incorrect and calls the function edit to restart the loop again
            print("incorrect please try again")
            edit() #calls the edit function to begin the loop again
        else:
            value = input("enter the new value: ")[:32] # asks the user which part of the selected task they like to edit then updates that bit of the task
        if not  value.islower(): # checks if the input is all lowercase to avoid a error
            print("only")

            cursor.execute(f"UPDATE tasks SET {cri} = ? WHERE taskid = ?", (value,search)).fetchall()
        view()
        print("your edited value was : " +value)
        connection.commit()  # commits the charges to the database
    else : # handles any error of a miss input or if the data does not exist
        print("Data does not exist")
        edit()

# wipe
def wipe(): #wipes all data from the database which allows for easier cleanup if the database ecounters any issues or has to many uneeded tasks
    cursor.execute("DELETE FROM tasks ;") # deletes everything with the table tasks
    print("table as been wiped") # informs the user the table has been wiped
#resets task id colllm
    cursor.execute("UPDATE sqlite_sequence SET SEQ=0 WHERE NAME ='tasks';") # updates the Coloum within the SQLITE_SEQUENCE table that keeps track of what number the auto increment is currently set to within the table tasks
    connection.commit()  # commits the charges to the database

#search
def search(): # allows the user to search by a certain criteria for specific data within the table
  try :
   criteria = input("what caterogry would like to search in:")[:8] # asks the user what they would collum they would like to search in
   search = input("what would you like to search for\n ")[:8] # asks what the user  they want to search by limts the size of the string that is stored 8 charaters to help migiate any sql injection attacks 8 charaters strikes a nice balance between secuirty and useablity
   answer =  cursor.execute(f"SELECT * from tasks WHERE {criteria}  = ?", (search)).fetchall() # selects everything with the table tasks that matches the value of search sqlite select everything that matches inputted value while search is vulnerable to sql injection its unlikley to ocur given the charater limit

   # tells the user the data they are looking for doesn't exist and to try again
   if not answer :
       print("data does not exist please try again")

   #prints the requested information
   else :
       print(answer)
 # error handles and informs the user their input was invaild and to try again
  except :
     print("invaild input please try again")

#exit
def exit(): # exits the program
    # displays a ranomised moviation message
    with codecs.open("messages.txt", "r", # opens the text file messages in a read only state no need to handle closing the file since with does that automatically
                     encoding="utf-8", # states the type of encoding as utf-8
                     errors="replace") as file: # handles any errors due to the symbol not being included in the utf-8 standard and replaces them
        lines = file.read().splitlines() # reads the file and splits the lines up then saves to the lines varible
        print(random.choice(lines)) # uses the random modules to pick a random string from the lines varible then print the one it picks
        with closing(sqlite3.connect("Tasks.db")) as connection: # closes the connection between this program and the database
            with closing(connection.cursor()) as cursor:()
            sys.exit(0) # exits the program


def main(): #displays the main menu of the program

    while True: # checks if the users input for what they want to do matches the one of the three options if not returns them and asks them again while displaying a message
        task = input("what do you want to: \n A) create\n B) view\n C) edit\n D) delete\n E) wipe\n F) search\n G) exit\n please enter here: ")[:32]# ask the user what they would like to do and limits the saved string length to 32 charters
        if task == "create":                                                                                                                              # if the user input matches any of these strings it calls the related function
            create()
        elif task == "view":
            view()
        elif task == "edit":
            edit()
        elif task == "delete":
            delete()
        elif task == "wipe":
            wipe()
        elif task == "search":
            search()
        elif task == "exit":
            exit()
        else:
            print("please enter a vaild response") # handles any errors caused by misinputs and informs the user of the error then while prompting them to input a vaild response

if __name__ == "__main__": # checks if the script being being run as the main program if it is then it runs main which allows the program to run and not get stuck at creating the table
    main()
